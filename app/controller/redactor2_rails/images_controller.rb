class Redactor2Rails::ImagesController < ApplicationController
  before_filter :redactor2_authenticate_user!

  def index
    image_data = []
    Redactor2Rails::Image.all.each do |image|
      image_data << {
        thumb: image.thumb,
        url: image.image,
        title: '',
        id: image.id
      }
    end

    render json: image_data
  end

  def create
    @image = Redactor2Rails.image_model.new

    file = params[:file]
    @image.data = Redactor2Rails::Http.normalize_param(file, request)
    if @image.has_attribute?(:"#{Redactor2Rails.devise_user_key}")
      @image.send("#{Redactor2Rails.devise_user}=", redactor_current_user)
      @image.assetable = redactor_current_user
    end

    if @image.save
      render json: { id: @image.id, url: @image.url(:content) }
    else
      render json: { error: @image.errors }
    end
  end

  private

  def redactor2_authenticate_user!
    if Redactor2Rails.image_model.new.has_attribute?(Redactor2Rails.devise_user)
      super
    end
  end
end
